#!/bin/bash

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see https://www.gnu.org/licenses

if [ $# -ne 1 ];
then
	echo "Not enough arguments. Need elasticsearch version number to proceed."
	echo -e "Usage: $(basename ${0}) <elasticsearch version number>\n"
	exit 1
fi

ES_VERSION=${1}

if [[ "$EUID" -ne 0 ]];
then
	echo -e "You need to be root to run this.\nExiting."
	exit 1
fi

echo "Downloading elasticsearch version ${ES_VERSION}"
if ! wget --quiet https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-oss-${ES_VERSION}-amd64.deb;
then
	echo "Download failed."
	exit 1
fi

echo "Downloading checksum file."
if ! wget --quiet https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-oss-${ES_VERSION}-amd64.deb.sha512;
then
	echo "Download failed."
	exit 1
fi

echo "Checking file integrity."
if ! shasum -a 512 -c elasticsearch-oss-${ES_VERSION}-amd64.deb.sha512 > /dev/null;
then
	echo "File corrupted."
	exit 1
else
	echo "File integrity: OK"
fi

echo "Installing elasticsearch-oss-${ES_VERSION}"
dpkg -i elasticsearch-oss-${ES_VERSION}-amd64.deb

echo "Installing ingest-attachment"
yes | /usr/share/elasticsearch/bin/elasticsearch-plugin install ingest-attachment

echo "Setting virtual memory map limit."
sysctl -w vm.max_map_count=262144

echo "Starting and enabling elasticsearch service."
systemctl enable --now elasticsearch

echo "Checking locale settings."
if ! locale | grep "LANG=" | grep "UTF-8" > /dev/null;
then
	echo "LANG=en_UK.UTF-8" > /etc/default/locale
else
	echo "locale: OK."
fi

echo "Installing required packages."
apt -y install curl apt-transport-https gnupg

echo "Installing repository key."
curl -fsSL https://dl.packager.io/srv/zammad/zammad/key | \
  gpg --dearmor | tee /etc/apt/trusted.gpg.d/pkgr-zammad.gpg > /dev/null

echo "deb [signed-by=/etc/apt/trusted.gpg.d/pkgr-zammad.gpg] https://dl.packager.io/srv/deb/zammad/zammad/stable/debian 11 main"| \
  tee /etc/apt/sources.list.d/zammad.list > /dev/null

echo "Installing Zammad."
apt update && apt -y install zammad

echo "Checking Zammad service."
if ! systemctl status zammad | grep "active (running)" > /dev/null;
then
	echo "Starting Zammad service."
	systemctl enable --now zammad
else
	echo "Zammad service: OK"
fi
 
echo "Setting Elasticsearch server address."
zammad run rails r "Setting.set('es_url', 'http://localhost:9200')"

echo "Building the search index."
zammad run rake zammad:searchindex:rebuild

echo "Removing default Nginx landing page."
rm /etc/nginx/sites-enabled/default
echo "Restarting Nginx service."
systemctl restart nginx

echo -e "Zammad is now installed and ready to use.\nHave fun!"
